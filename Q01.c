#include <stdio.h>
#include <string.h>

void revMethodZero(char sen[]);
void revMethodOne(char sen[]);

int main()
{
    char sen[100];
    printf("Enter a sentence to get the reversed one : ");
    scanf("%[^\n]",sen);
    printf("Choose a method (Reverse words - 0/ Reverse letters - 1) : ");
    int choice;
    scanf("%d",&choice);
    printf("\nReversed sentence : ");

    if(choice)
    {
        revMethodOne(sen);
    }
    else
    {
        revMethodZero(sen);
    }
    printf("\n");

    return 0;
}

void revMethodZero(char sen[])
{
    int l = strlen(sen);
    int end = l-1;
    for(int i=end; i>=0; i--)
    {
        if(sen[i]==' ')
        {
            for(int j = i+1; j<=end; j++)
            {
                printf("%c",sen[j]);
            }
            printf(" ");
            end = i-1;
        }
    }
    for(int x = 0; x<=end; x++)
    {
        printf("%c",sen[x]);
    }

}

void revMethodOne(char sen[])
{
    int l = strlen(sen);
    for(int i = l-1; i>=0; i--)
    {
        printf("%c",sen[i]);
    }
}
