#include <stdio.h>

int r,c;
void add(int arr1[r][c],int arr2[r][c]);
void mul(int arr1[r][c],int arr2[r][c]);

int main()
{
    printf("First enter the size of matrices.\n");
    printf("Number of rows : ");
    scanf("%d",&r);
    printf("Number of columns : ");
    scanf("%d",&c);
    int arr1[r][c], arr2[r][c];
    printf("\nEnter the elements of the matrices row by row.\nFirst matrix : \n");
    for (int i = 0;i<= (r-1);i++){
        for(int j = 0;j<= (c-1);j++){
            scanf("%d",&arr1[i][j]);
        }
    }
    printf("Second matrix : \n");
    for (int i = 0;i<= (r-1);i++){
        for(int j = 0;j<= (c-1);j++){
            scanf("%d",&arr2[i][j]);
        }
    }

    printf("Select the mathematical operator (Add - '+' or Multiply - '*') : ");
    char choice;
    scanf("\n%c",&choice);

    switch(choice){
    case '+':
        printf("Matrix 1 + Matrix 2 is :\n");
        add(arr1,arr2);
        break;
   case '*':
       printf("Matrix 1 * Matrix 2 is :\n");
        mul(arr1,arr2);
       break;
    default:
        printf("Invalid operator!");
    }



    return 0 ;
}

void add(int arr1[r][c],int arr2[r][c]){

    for (int i = 0;i<= (r-1);i++){
        for(int j = 0;j<= (c-1);j++){
            printf("%d ",arr1[i][j]+arr2[i][j]);
        }
        printf("\n");
    }

}

void mul(int arr1[r][c],int arr2[r][c]){

    int result[r][c];
    for (int i = 0;i<= (r-1);i++){
        for(int j = 0;j<= (c-1);j++){
            result[i][j] = 0;
            for (int k = 0; k < (c-1); k++){
                result[i][j] += (arr1[i][k] * arr2[k][j]);
            }
        }

    }
    for(int a=0;a<=(r-1);a++){
        for(int b=0;b<=(c-1);b++){
            printf("%d ",result[a][b]);
        }
        printf("\n");
    }

}
