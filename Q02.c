#include<stdio.h>
#include<string.h>

int main()
{
    char str[100],ch;
    int count = 0;
    printf("Enter the string : ");
    scanf("%[^\n]",str);
    printf("Enter a character to get the frequency : ");
    scanf("\n%c",&ch);
    int l = strlen(str)-1;
    for(int i = 0; i<=l; i++)
    {
        if(str[i]==ch)
        {
            count++;
        }
    }
    printf("\n\nThe character '%c' found %d times in the string.\n",ch,count);


    return 0;
}
